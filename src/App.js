import React from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import Login from "./Components/Login/Login";
import Navbar from "./Components/Navbar/Navbar";
import Profile from "./Components/profile/Profile";
import Translation from "./Components/Translation/Translation";
import TranslationBox from "./Components/Translation/TranslationBox";

function App() {
    return (
        <div className="App">
            <Navbar />
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/profile" element={<Profile />} />
                <Route path="translations" element={<Translation />} />
                <Route path="translations/:id" element={<TranslationBox />} />
            </Routes>
        </div>
    );
}

export default App;
