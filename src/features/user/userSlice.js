import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

//api url && key

// our intialstate
const initialState = {
    id: 0,
    username: "",
    translations: [],
    loggedIn: false,
};

//to add post
export const newAddPost = createAsyncThunk("user/newAddPost", async (data) => {
    const api_url = process.env.REACT_APP_API_URL;
    const api_key = process.env.REACT_APP_API_KEY;
    fetch(`${api_url}`, {
        method: "POST",
        headers: {
            "X-API-Key": api_key,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            ...data,
        }),
    }).then((response) => {
        if (!response.ok) {
            throw new Error("Could not update translations history");
        }
        return response.json();
    });
});

// to update post
export const updatePost = createAsyncThunk("user/updatePost", async (data) => {
    const api_url = process.env.REACT_APP_API_URL;
    const api_key = process.env.REACT_APP_API_KEY;
    fetch(`${api_url}/${data.id}`, {
        method: "PATCH",
        headers: {
            "X-API-Key": api_key,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            ...data,
        }),
    }).then((response) => {
        if (!response.ok) {
            throw new Error("Could not update translations history");
        }
        return response.json();
    });
});

export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        //getting username
        set_user: (state, action) => {
            state.username = action.payload;
        },
        //authentiction is set to true
        authenticated: (state) => {
            state.loggedIn = true;
        },
        // logout state
        logout: (state) => {
            state.loggedIn = false;
        },
    },
});

// our exports for username && loggedin
export const selectUser = (state) => state.user.loggedIn;
export const user_name = (state) => state.user.username;

//exporting to use in components
export const { authenticated, logout, set_user } = userSlice.actions;

export default userSlice.reducer;
