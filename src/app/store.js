import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../features/user/userSlice";

//our store for our reducer
export const store = configureStore({
    reducer: {
        user: userReducer,
    },
});
