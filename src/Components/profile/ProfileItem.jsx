import React from "react";

function ProfileItem({ word, index }) {
    //split the array with words, so we can loop through and render the images.
    let chars = word.split("");

    return (
        <div className="profileItemWrapper row  ">
            <div className="col-sm-6 ">
                <p className="transWord ">{word}</p>
            </div>
            <div className="col-sm-6">
                {chars &&
                    chars.map((char, i) => {
                        return char === " " ? null : (
                            <img
                                key={i}
                                className="profilePictures"
                                height={30}
                                src={`${require("../../assets/" +
                                    char +
                                    ".png")}`}
                                alt={`Sign language signal for letter ${char}`}
                            />
                        );
                    })}
            </div>
        </div>
    );
}

export default ProfileItem;
