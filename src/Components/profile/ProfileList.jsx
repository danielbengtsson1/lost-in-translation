import React from "react";
import ProfileItem from "./ProfileItem";
import "./profile.css";

function ProfileList({ auth }) {
    //Variables
    let session;
    let wordArray;
    let lastTenTranslations;
    //Auth state. that comes from parent. if user exists. update session, and save the translations into separate variable. then slice away to only show the 10 latest translations.
    if (auth) {
        session = JSON.parse(localStorage.getItem("user"));
        wordArray = session.translations;
        lastTenTranslations = wordArray.slice(-10);
    }
    return (
        <div className="container row">
            {lastTenTranslations &&
                lastTenTranslations.map((word, i) => {
                    return (
                        <div className=" col-sm-12 " key={i}>
                            <ProfileItem word={word} index={i} />
                        </div>
                    );
                })}
        </div>
    );
}

export default ProfileList;
