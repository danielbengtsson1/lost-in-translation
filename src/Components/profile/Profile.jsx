import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectUser } from "../../features/user/userSlice";
import { useNavigate } from "react-router-dom";
import ProfileList from "./ProfileList";
import { updatePost } from "../../features/user/userSlice";
import { useDispatch } from "react-redux";
function Profile() {
    //Hooks
    const auth = useSelector(selectUser);
    const [reRender, setReRender] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    //If user is not authenticated, redirect to login.
    useEffect(() => {
        if (!auth) {
            navigate("/");
        }
    }, [auth]);

    //Get the current session from localstorage
    let session = JSON.parse(localStorage.getItem("user"));

    //function that delets all the translations. also updates the localstorage. and render a notification to the user.
    const deleteTranslations = () => {
        const userObject = {
            id: session.id,
            username: session.username,
            translations: [],
        };
        dispatch(updatePost(userObject));
        localStorage.setItem("user", JSON.stringify(userObject));
        setReRender(true);
        setTimeout(() => {
            setReRender(false);
        }, 2000);
    };

    return (
        <div className="container mt-5">
            <ProfileList auth={auth} />
            {reRender ? (
                <div className="bg-success text-center p-2 border-radius">
                    <p>Translations removed</p>
                </div>
            ) : null}
            <button
                className="btn btn-danger mt-5"
                onClick={deleteTranslations}
            >
                Delete translations
            </button>
        </div>
    );
}

export default Profile;
