import React, { useEffect } from "react";
import { useState } from "react";
import "./translation.css";
import TranslationBox from "./TranslationBox";

import { updatePost } from "../../features/user/userSlice";
import { useDispatch } from "react-redux";

const TranslationInput = () => {
    //states
    const [inputValue, setInputValue] = useState("");
    const [renderTranslations, setRenderTranslation] = useState(false);

    const dispatch = useDispatch();
    //array for splitet string
    let chars = [];

    useEffect(() => {
        setRenderTranslation(false);
    }, [inputValue]);

    //handling the input
    const handleInputValue = (e) => {
        setInputValue(e.target.value);
    };

    //Submiting our input value
    const onSubmit = (e) => {
        e.preventDefault();
        //if input value is not empty.
        if (inputValue !== "") {
            //splting our string
            let charsTmp = inputValue.toLowerCase().split("");

            //iterate it thorugh
            for (let i = 0; i < charsTmp.length; i++) {
                if (charsTmp[i] !== " " && !/[^a-zA-Z]/.test(charsTmp[i])) {
                    //pushin to our array
                    chars.push(charsTmp[i]);
                }
            }
            //geting our users from localstorage
            let session = JSON.parse(localStorage.getItem("user"));
            //joining our splited string
            session.translations.push(chars.join(""));
            //creating our object and adding our localstorage data
            const userObject = {
                id: session.id,
                username: session.username,
                translations: session.translations,
            };
            //sending to our path
            dispatch(updatePost(userObject));
            //setting it to local storage
            localStorage.setItem("user", JSON.stringify(userObject));
            //setting it to thorugh
        }

        setRenderTranslation(true);
    };

    return (
        <>
            <div className="container d-flex mt-5 justify-content-center">
                <form className="form-outline" onSubmit={onSubmit}>
                <label className="label label-default text-uppercase text-dark mb-3">
						Translate from text to sign language{' '}
					</label>
                    <input
                      className="form-control mb-3"
                        type="text"
                        onChange={handleInputValue}
                        placeholder="Enter a word to translate"
                    />
                    <button className="btn btn-primary" type="submit">
                        Translate
                    </button>
                </form>
            </div>
            <div className="mt-5">
                {/* if true we show and propdrilling to translationBox */}
                {renderTranslations ? (
                    <TranslationBox inputValue={inputValue} />
                ) : null}
            </div>
        </>
    );
};

export default TranslationInput;
