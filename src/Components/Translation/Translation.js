import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectUser } from "../../features/user/userSlice";
import TranslationInput from "./TranslationInput";

const Translation = () => {
    //navigate and authorized user
    const navigate = useNavigate();
    const auth = useSelector(selectUser);

    useEffect(() => {
        //checking if user is auth
        if (!auth) {
            navigate("/");
        }
    }, [auth]);
    return (
        <div className="text-center">
            <TranslationInput />
        </div>
    );
};

export default Translation;
