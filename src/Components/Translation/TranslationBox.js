import React from "react";

//getting our prop
const TranslationBox = ({ inputValue }) => {
    //we split the input value string
    let strToTranslate = inputValue.toLowerCase().split("");
    let chars = [];

    //iterate && and taking away spaces so we don't get a error 
    for (let i = 0; i < strToTranslate.length; i++) {
        if (strToTranslate[i] !== " " && !/[^a-zA-Z]/.test(strToTranslate[i])) {
            //pushing to the array
            chars.push(strToTranslate[i]);
        }
    }

    return (
        <div className="d-flex justify-content-center">

        {/* iterate throgh and assigning the values to img */}
            {chars.map((letter, i) => {
                return (
                    <img
                        className='img-fluid z-depth-2 rounded' style={{width:"5rem"}}
                        key={i}
                        src={`${require("../../assets/" + letter + ".png")}`}
                        alt={`Sign language signal for letter ${letter}`}
                    />
                );
            })}

            
        </div>
    );
};

export default TranslationBox;
