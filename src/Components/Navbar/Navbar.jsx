import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    logout,
    selectUser,
    set_user,
    user_name,
} from "../../features/user/userSlice";
import { NavLink } from "react-router-dom";
import "./navbar.css";

const Navbar = () => {
    //Hooks
    const dispatch = useDispatch();
    const auth = useSelector(selectUser);
    const userName = useSelector(user_name);
    const session = JSON.parse(localStorage.getItem("user"));

    useEffect(() => {
        if (auth) {
            dispatch(set_user(session.username));
        }
    }, [auth]);

    //Function to logout users. removes localstorage, and dispatch to redux to set login state to false.
    const logoutOutUser = () => {
        localStorage.removeItem("user");
        dispatch(logout());
    };

    return (
        <div className="navBar row pt-4 px-4 ">
            <div className="col-md-8">
                <h1 className="">
                    Welcome <span>{auth && auth ? userName : null}</span>
                </h1>
            </div>

            {auth && auth ? (
                <div className="col-md-4 d-flex justify-content-between  align-self-center">
                    <NavLink className="link" to="/profile">
                        Profile
                    </NavLink>
                    <NavLink className="link" to="/translations">
                        translations
                    </NavLink>

                    <button className="btn btn-warning" onClick={logoutOutUser}>
                        logout
                    </button>
                </div>
            ) : null}
        </div>
    );
};

export default Navbar;
