import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import {
    authenticated,
    set_user,
    newAddPost,
} from "../../features/user/userSlice";

function InputField() {
    //UseStates
    const [inputValue, setInputValue] = useState("");

    //react redux hook
    const dispatch = useDispatch();

    //Function that triggers when the users has pressed the login button
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = await checkUser(inputValue);
        if (data.length === 0) {
            createUser(inputValue);
            dispatch(authenticated());
            dispatch(set_user(inputValue));
        } else {
            dispatch(set_user(inputValue));
            logInExistingUser(inputValue);
        }
    };

    //Function that handles the value from the input field
    const handleInput = (e) => {
        setInputValue(e.target.value);
    };

    //Getting user from API.
    const checkUser = async (name) => {
        const api_url = process.env.REACT_APP_API_URL;
        try {
            const response = await fetch(`${api_url}?username=${name}`);
            if (!response.ok) {
                throw new Error("could not complete reqeusts");
            }
            const data = await response.json();
            return data;
        } catch (error) {
            return [error.message, []];
        }
    };

    //function thats using checkuser function^^ if we get a return with length bigger then 0, then we set the that fetched user to localstorage and also log them in through redux.
    const logInExistingUser = async (userData) => {
        const data = await checkUser(userData);
        if (data.length > 0) {
            localStorage.setItem("user", JSON.stringify(...data));
            dispatch(authenticated());
        }
    };
    //this function creates a user and posts that user to the api also sets a "token" in the localstorage.
    const createUser = (user) => {
        const userObject = {
            id: Math.floor(Math.random() * 10000),
            username: user,
            translations: [],
        };
        localStorage.setItem("user", JSON.stringify(userObject));
        dispatch(newAddPost(userObject));
    };

    return (
        <>
            <div className=" mb-3 ">
                <form className="d-flex" onSubmit={handleSubmit}>
                    <input
                        onChange={handleInput}
                        type="text"
                        className={`form-control border`}
                        placeholder="What's your name?"
                        aria-label="What's your name?"
                        aria-describedby="basic-addon2"
                    />
                    <div className="btnWrapper">
                        <button className={`btn btn-primary`}>Login</button>
                    </div>
                </form>
            </div>
        </>
    );
}

export default InputField;
