import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authenticated, selectUser } from "../../features/user/userSlice";
import InputField from "./InputField";

import { useNavigate } from "react-router-dom";
import "./login.css";

function Login() {
    //get the authentication from redux
    const auth = useSelector(selectUser);
    const navigate = useNavigate();

    const dispatch = useDispatch();

    //Check if users has session, redirect if true.
    useEffect(() => {
        if (auth === true) {
            navigate("/translations");
        }

        checkLocalStorage();
    }, [auth]);

    //Function that checks if we have a session, if we have a session, dispatch it to redux.
    const checkLocalStorage = () => {
        const session = JSON.parse(localStorage.getItem("user"));
        if (session) {
            dispatch(authenticated());
        }
    };

    return (
        <div className=" login pt-5 text-center">
            <h1>Lost in translation</h1>
            <h3>Get started</h3>
            <div className="inputWrapper container">
                <InputField />
            </div>
        </div>
    );
}

export default Login;
