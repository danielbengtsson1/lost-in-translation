## Usage

.env in root directory.
env variables:
REACT_APP_API_URL=""
REACT_APP_API_KEY=""

When the user enters the site, they are prompted with an inputfield, where they can enter there name. if they have already visited the site and entered there name once before, they will be logged in to there existing account. 

Once the user is logged in, they can now start entering words they want to be translated from text to sign language. once translated they will be shown on the profile page. where they can see the 10 last translations they did. they can also choose do delete all the translations they have created. 

## Developers

Elmin Nurkic - elmin.nurkic@se.experis.com
Daniel Bengtsson - daniel.johan.gunnar.bengtsson@se.experis.com

## Documentation

This application is created with the react library with redux toolkit.
We are using redux for global state management and bootstrap as our css framework to speed up the design process. 
We are fetching and posting data to an API to be able to save data.
we used trello to plan and keep track of our work. 

We have also used figma to create a component tree.
![component-tree](./component-tree.pdf)
# git

we are using first letter of name and first letter of last name and id from trello, and name of errand. ex db#1-start when creating branches.
![image-1.png](./image-1.png)

## Problems

We tried using a higher order component with authentication, but it created a render issue. and we didn't have time to fix it.
